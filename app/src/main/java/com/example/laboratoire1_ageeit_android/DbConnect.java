package com.example.laboratoire1_ageeit_android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import static androidx.core.content.ContextCompat.startActivity;

public class DbConnect extends AsyncTask {

    final private Context context;
    private String nomUtilisateur;
    private AlertDialog alertDialog;



    public DbConnect(final Context c)
    {
        this.context=c;
    }
    @Override
    protected void onPreExecute()
    {

        this.alertDialog=new AlertDialog.Builder(this.context).create();
        this.alertDialog.setTitle("Statut du login");
    }
    @Override
    protected void onPostExecute(Object o)
    {
        int r=Integer.parseInt(o.toString().trim());
              if(r!=1)
    {
        this.alertDialog.setMessage("Informations invalides !!!");
        this.alertDialog.show();
    }
        else
        {

            Intent it=new Intent(context,menu.class);
            it.putExtra("user",nomUtilisateur);
            context.startActivity(it);
        }
    }

    @Override
    protected String doInBackground(Object[] param)
    {


        nomUtilisateur=param[0].toString();
        String srciptCible="http://192.168.179.1/Android/script.php";
        try{
            URL url=new URL(srciptCible);
            HttpURLConnection con=(HttpURLConnection)url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("POST");

            OutputStream out=con.getOutputStream();
            BufferedWriter writer=new BufferedWriter(new OutputStreamWriter(out,"utf-8"));

            String message= URLEncoder.encode("user","utf-8")+"="+
                    URLEncoder.encode((String)param[0],"utf-8")+
                    "&"+URLEncoder.encode("pw","utf-8")+"="+
                    URLEncoder.encode((String)param[1],"utf-8");
            writer.write(message);
            writer.flush();
            writer.close();
            out.close();

            InputStream input=con.getInputStream();
            BufferedReader lecteur=new BufferedReader(new InputStreamReader(input,"iso-8859-1"));
            String ligne;
            StringBuffer stringBuffer=new StringBuffer();
            while((ligne=lecteur.readLine())!=null)
            {
                stringBuffer.append(ligne+"\n");
            }
            return stringBuffer.toString();
        }
        catch (Exception ex)
        {
            return ex.getMessage();
        }
    }
}
