package com.example.laboratoire1_ageeit_android;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class modificationInformation extends AppCompatActivity implements View.OnClickListener {
    private EditText nomAModifier,prenomAModifier, usernameAModifier ,motDePasseAModifier ,motDePasse2AModifier;
    private Button btnModifierInfo;
    private TextView btnRetourMenu,txtResume;
    private String user;
    private Toast toast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modification_information);
        nomAModifier =(EditText)findViewById(R.id.nomAModifier);
        prenomAModifier =(EditText)findViewById(R.id.prenomAModifier);
        usernameAModifier =(EditText)findViewById(R.id.usernameAModifier);
        motDePasseAModifier =(EditText)findViewById(R.id.motDePasseAModifier);
        motDePasse2AModifier =(EditText)findViewById(R.id.motDePasse2AModifier);
        btnRetourMenu =(TextView) findViewById(R.id.btnRetourMenu);
        txtResume=(TextView) findViewById(R.id.txtResume);
        btnModifierInfo =(Button) findViewById(R.id.btnModifierInfo);
        nomAModifier =(EditText)findViewById(R.id.nomAModifier);

        Intent  it=getIntent();
        user =it.getStringExtra("user");
        recupererInfoByUser(user);

    }
    @Override
    public void onClick(View view)
    {

    }
    public void recupererInfoByUser(String user)
    {

        String srciptCible="http://192.168.179.1/Android/select.php";
        try{
            URL url=new URL(srciptCible);
            HttpURLConnection con=(HttpURLConnection)url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("POST");

            OutputStream out=con.getOutputStream();
            //oN IRAIT LE PROGRAMME N'ARRIVE PAS ICI
            BufferedWriter writer=new BufferedWriter(new OutputStreamWriter(out,"utf-8"));

            String message= URLEncoder.encode("user","utf-8")+"="+
                            URLEncoder.encode(user,"utf-8");
            writer.write(message);
            writer.flush();
            writer.close();
            out.close();

            InputStream input=con.getInputStream();
            BufferedReader lecteur=new BufferedReader(new InputStreamReader(input,"iso-8859-1"));
            String ligne;
            StringBuffer stringBuffer=new StringBuffer();
            txtResume.setText("AU DESSUS DU WHILE ");
            while((ligne=lecteur.readLine())!=null)
            {
                stringBuffer.append(ligne+"/n");
            }
            txtResume.setText("Retour "+stringBuffer.toString());
            toast= Toast. makeText(getApplicationContext(),"Retour : "+stringBuffer.toString(),Toast. LENGTH_SHORT);
            toast. setMargin(50,50);
            toast. show();
        }
        catch (Exception ex)
        {
            //txtResume.setText("Soucis "+ex.getMessage());
            toast= Toast. makeText(getApplicationContext(),"SOUCIS : "+ex.getMessage(),Toast. LENGTH_SHORT);
            toast. setMargin(50,50);
            toast. show();
        }
    }
    }

