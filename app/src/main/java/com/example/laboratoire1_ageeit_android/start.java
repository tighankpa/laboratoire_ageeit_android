package com.example.laboratoire1_ageeit_android;

import android.content.Intent;
import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import static androidx.core.content.ContextCompat.startActivity;

public class start extends AppCompatActivity {
    private Context context;
    public start(Context c)
    {
        this.context=c;
    }
    public void startMenu(String nom)
    {
        Intent it=new Intent(context,menu.class);
        it.putExtra("nom",nom);
        startActivity(it);
    }

}
