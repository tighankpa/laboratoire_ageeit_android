package com.example.laboratoire1_ageeit_android;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class menu extends AppCompatActivity  implements View.OnClickListener{

    private CardView escalade, natation, course, randonnee;
    private Button btnModifier, btnProposer;
    private Toast toast;
    private String user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        escalade =(CardView)findViewById(R.id.escalade);
        natation =(CardView)findViewById(R.id.natation);
        course =(CardView)findViewById(R.id.course);
        randonnee =(CardView)findViewById(R.id.randonnee);
        btnModifier =(Button)findViewById(R.id.btnModifier);
        btnProposer =(Button)findViewById(R.id.btnProposer);
        escalade.setOnClickListener(this);
        natation.setOnClickListener(this);
        course.setOnClickListener(this);
        randonnee.setOnClickListener(this);
        btnModifier.setOnClickListener(this);
        btnProposer.setOnClickListener(this);

        Intent it=getIntent();
        user=it.getStringExtra("user");


    }
    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.escalade:

                toast=Toast. makeText(getApplicationContext(),"ESCALADE",Toast. LENGTH_SHORT);
                toast. setMargin(50,250);
                toast. show();
            break;

            case R.id.natation:
                 toast= Toast. makeText(getApplicationContext(),"NATATION",Toast. LENGTH_SHORT);
                toast. setMargin(50,50);
                toast. show();
                break;

            case R.id.course:
                 toast= Toast. makeText(getApplicationContext(),"COURSE",Toast. LENGTH_SHORT);
                toast. setMargin(50,50);
                toast. show();
                break;

            case R.id.randonnee:
                toast= Toast. makeText(getApplicationContext(),"RANDONEE",Toast. LENGTH_SHORT);
                toast. setMargin(50,50);
                toast. show();
            break;
            case R.id.btnModifier:

                toast= Toast. makeText(getApplicationContext(),user,Toast. LENGTH_SHORT);
                toast. setMargin(50,50);
                toast. show();
                Intent activityModification= new Intent(this,modificationInformation.class);
                activityModification.putExtra("user",user);
                startActivity(activityModification);
            break;
            case R.id.btnProposer:

            break;
        }


    }

}
