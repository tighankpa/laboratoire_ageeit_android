package com.example.laboratoire1_ageeit_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.sql.Connection;

public class MainActivity extends AppCompatActivity {
    private EditText user;
    private EditText password;
    private TextView inscriptionLien;
    private Button btnConnect;
    private  Intent connect;
    private  Intent intentToInscription;
    private  Intent intentToMenu;
    Connection con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user=(EditText)findViewById(R.id.LoginUsername);
         password=(EditText)findViewById(R.id.LoginMotDePasse);
        inscriptionLien=(TextView)findViewById(R.id.inscriptionLien);
        btnConnect=(Button)findViewById(R.id.btnConnecter);
         connect=new Intent(this,Inscription.class);
        intentToInscription=new Intent(this,Inscription.class);
       intentToMenu=new Intent(this,menu.class);

        inscriptionLien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intentToInscription);
            }
        });
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Connexion(user.getText().toString(),password.getText().toString());
            }
        });


    }
    public void Connexion(String user,String pass)
    {
        DbConnect operateur= new DbConnect(this);
        operateur.execute(user,pass);
    }

}
